import cv2
import numpy as np
import rospy
from hector_uav_msgs.srv import EnableMotors
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

cv_bridge_inst = CvBridge()

ALTITUDE_DESIRED_VALUE = 5.0
kz = 1.0
bz = 2.0

ks = 0.01
bs = 0.005

ky = 0.004
by = 0.00005

class Controller:
    def __init__(self):
        rospy.init_node("controller_node")
        rospy.Subscriber("/ground_truth/state", Odometry, self.state_callback)
        rospy.Subscriber("/cam_1/camera/image", Image, self.camera_callback)
        self.cmd_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
        self.position = Point()
        self.twist = Twist()
        self.omega_error = 0
        self.omega_error_prev = 0
        self.e_x_blue, self.e_x_blue_prev = 0, 0
        self.y_error, self.y_error_prev = 0, 0
        self.blue_ring_detected = False
        self.red_ring_detected = False

        rospy.on_shutdown(self.stop_robot)
   
    def __del__(self):
      self.stop_robot()

    def stop_robot(self):
        cmd_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
        cmd_msg = Twist()
        cmd_msg.linear.x = 0
        cmd_msg.linear.y = 0
        cmd_msg.linear.z = 0
        cmd_msg.angular.z = 0
        cmd_pub.publish(cmd_msg)
        self.cmd_pub.publish(cmd_msg)
        

    def state_callback(self, msg):  
        self.position = msg.pose.pose.position
        self.twist = msg.twist.twist
    
    def ring_detector(self, image, lower, upper, color):
        color_mask = cv2.inRange(image, lower, upper)
        color_contours, _ = cv2.findContours(color_mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        if color_contours:
           max_len_c = 0
           c = color_contours[0]
           for i in range(0, len(color_contours)):
               if len(color_contours[i]) > max_len_c:
                   c = color_contours[i]
                   max_len_c = len(color_contours[i])
           self.color_distance = max_len_c
           M = cv2.moments(c)
           if M['m00'] != 0:
               cx = int(M['m10']/M['m00'])
               cy = int(M['m01']/M['m00'])
           else:
               cx = 0
               cy = 0
           (x1,y1), color_r = cv2.minEnclosingCircle(c)
           if color_r > 10:
               image = cv2.circle(image, (cx, cy), radius=5, color=color, thickness=-1)
               cv2.drawContours(color_r, c, -1, (0,255,0), 1)
               color_r = cv2.circle(color_r, (int(x1), int(y1)), radius=int(color_r), color=color, thickness=4)       
               return image, (x1,y1), color_r[0]
               return image, (0,0), 0


    def camera_rings_callback(self, msg):
       # """ Computer vision stuff for Rings"""
       try:
           cv_image = self.cv_bridge.imgmsg_to_cv2(msg, "bgr8")
       except CvBridgeError as e:
           rospy.logerr("CvBridge Error: {0}".format(e))


       # red
       lower = np.uint8([0, 0, 90])
       upper = np.uint8([30, 30, 120])
       cv_image, red_pose, red_radius  = self.ring_detector(cv_image, lower, upper, (0,0,255))


       # blue
       lower = np.uint8([40, 20, 20])
       upper = np.uint8([80, 50, 50])
       cv_image, blue_pose, blue_radius = self.ring_detector(cv_image, lower, upper, (255,0,0))


       # print(red_radius, blue_radius)


       if 50 < red_radius < 70 or 50 < blue_radius < 80:
           if red_radius > blue_radius:
               self.blue_ring_detected = False
               self.red_ring_detected = True
           else:
               self.red_ring_detected = False
               self.blue_ring_detected = True
              
               # offset in ring xy-plane to fly through center of a ring
               # error = <center of image> - <center of ring>
               self.e_x_blue = 160 - blue_pose[0]
               self.e_y_blue = 120 - blue_pose[1]
       else:
           self.blue_ring_detected = False
           self.red_ring_detected = False


       # save results
       self.image_2 = cv_image


    def camera_callback(self, msg):
        try:
            cv_image = cv_bridge_inst.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            rospy.logerr("CvBridge Error: {0}".format(e))

        # Implement color detection logic for blue and red rings
        blue_lower = np.array([100, 100, 100])
        blue_upper = np.array([140, 255, 255])
        red_lower = np.array([0, 0, 90])
        red_upper = np.array([30, 30, 120])

        blue_ring_mask = cv2.inRange(cv_image, blue_lower, blue_upper)
        red_ring_mask = cv2.inRange(cv_image, red_lower, red_upper)

        blue_ring_detected = cv2.countNonZero(blue_ring_mask)
        red_ring_detected = cv2.countNonZero(red_ring_mask)

        if blue_ring_detected > 1000:  # Adjust the threshold value based on your image
            self.blue_ring_detected = True
        else:
            self.blue_ring_detected = False

        if red_ring_detected > 1000:  # Adjust the threshold value based on your image
            self.red_ring_detected = True
        else:
            self.red_ring_detected = False

        # Implement logic to calculate offset for blue ring
        if self.blue_ring_detected:
            cy_list = np.where(blue_ring_mask >= 10)[0]
            if len(cy_list) > 0:
                cy = int(np.average(cy_list))
                self.e_x_blue = 160 - cy  # Offset in x-direction to fly through the center of the blue ring

    def takeoff_ctrl(self, z_des):
        return kz * (ALTITUDE_DESIRED_VALUE - self.position.z) - bz * self.twist.linear.z
     
    def steering_strl(self):
        u_s =  ks * self.omega_error - bs * (self.omega_error - self.omega_error_prev) / (1.0 / 50.0)
        self.omega_error_prev = self.omega_error
        return u_s
   
    def y_offset_ctrl(self):
      u_y = ky * self.y_error - by * (self.y_error - self.y_error_prev) / (1.0 / 50.0)
      self.y_error_prev = self.y_error
      return u_y
   
    def spin(self):
        self.enable_motors()
        try:
            rate = rospy.Rate(50.0)
            while not rospy.is_shutdown():
               u_z = self.takeoff_ctrl(ALTITUDE_DESIRED_VALUE)
               u_s = self.steering_strl()
               u_y = self.y_offset_ctrl()

               cmd_msg = Twist()

               cmd_msg.linear.z = u_z
               cmd_msg.linear.x = 2.0
               cmd_msg.linear.y = u_y
               cmd_msg.angular.z = -u_s

               self.cmd_pub.publish(cmd_msg)


             

            if self.red_ring_detected:
                    # Implement logic to avoid red rings (for example, turn left or right)
                    cmd_msg = Twist()
                    cmd_msg.linear.z = u_z
                    cmd_msg.angular.z = 0.5  # Rotate right to avoid the red ring
                    self.cmd_pub.publish(cmd_msg)
            elif self.blue_ring_detected:
                    # Implement logic to fly through the blue ring (adjust y-offset)
                    u_y = ky * self.e_x_blue - by * (self.e_x_blue - self.e_x_blue_prev) / (1.0 / 50.0)
                    self.e_x_blue_prev = self.e_x_blue
                    cmd_msg = Twist()
                    cmd_msg.linear.z = u_z
                    cmd_msg.linear.y = u_y
                    self.cmd_pub.publish(cmd_msg)
            else:
                    # Implement default behavior when no rings are detected (hover at desired altitude)
                    cmd_msg = Twist()
                    cmd_msg.linear.z = u_z
                    self.cmd_pub.publish(cmd_msg)

            rate.sleep()

        except rospy.ROSInterruptException:
            self.stop_robot()
            print("Stop the robot!")

    def enable_motors(self):
        rospy.wait_for_service("/enable_motors")
        try:
            enable_motors = rospy.ServiceProxy("/enable_motors", EnableMotors)
            enable_motors(True)
            print("Motors started!")
        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)

def main():
    ctrl = Controller()
    ctrl.spin()

if __name__ == "__main__":
    main()