import cv2
import numpy as np
import rospy
from sensor_msgs.msg import Image

RING_AVOIDANCE_TIME = 5 # [seconds]
DEFAULT_ALTITUDE = 3 # [meters]

from hector_uav_msgs.srv import EnableMotors
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point

import tf.transformations as ttt

from cv_bridge import CvBridge, CvBridgeError

cv_bridge_inst = CvBridge()

ALTITUDE_DESIRED_VALUE = 5.0
kz = 1.0
bz = 2.0

ks = 0.01
bs = 0.005

ky = 0.008
by = 0.00005

class Controller:
  
  def __init__(self):
    rospy.init_node("controller_node")

    rospy.Subscriber("/ground_truth/state", Odometry, self.state_callback)

    rospy.Subscriber("/cam_1/camera/image", Image, self.camera_callback)

    self.cmd_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)

    self.position = Point()
    self.twist = Twist()
    self.omega_error = 0
    self.omega_vel = 0
    self.omega_error_prev = 0
    self.y_error = 0
    self.y_error_prev = 0

    rospy.on_shutdown(self.stop_robot)

  def __del__(self):
      self.stop_robot()

     
  def stop_robot(self):
      
      cmd_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
      cmd_msg = Twist()
      cmd_msg.linear.x = 0
      cmd_msg.linear.y = 0
      cmd_msg.linear.z = 0
      cmd_msg.angular.z = 0
      cmd_pub.publish(cmd_msg)

      self.cmd_pub.publish(cmd_msg)

  def state_callback(self, msg):  
    self.position = msg.pose.pose.position
    self.twist = msg.twist.twist

  def camera_callback(self, msg):
      """ Computer vision stuff"""
      try:
            cv_image = cv_bridge_inst.imgmsg_to_cv2(msg, "bgr8")
      except CvBridgeError as e:
            rospy.logerr("CvBridge Error: {0}".format(e))

      grey_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
      _, mask = cv2.threshold(grey_image, 8, 255, cv2.THRESH_BINARY_INV)
      cv_image = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)

      cv2.line(cv_image, (160, 0), (160, 240), (0, 123, 0), 1)
      cv2.line(cv_image, (0, 120), (320, 120), (0, 123, 0), 1)

      # "steering" conrol
      top_points = np.where(mask[10] >= 10)
      mid_points = np.where(mask[msg.height / 2] >= 10)
      if  (not np.isnan(np.average(top_points)) and not np.isnan(np.average(mid_points))):
            top_line_point = int(np.average(top_points))
            mid_line_point = int(np.average(mid_points))
            self.omega_error = top_line_point - mid_line_point
            
            cv2.circle(cv_image, (top_line_point, 10), 5, (0,0,255), 1)
            cv2.circle(cv_image, (mid_line_point, int(msg.height/2)), 5, (0,0,255), 1)
            cv2.line(cv_image, (mid_line_point, int(msg.height/2)), (top_line_point, 10), (0, 0, 255), 3)

      # y-offset control
      __, cy_list = np.where(mask >= 10)
      if not np.isnan(np.average(cy_list)):
            cy = int(np.average(cy_list))
            self.y_error = msg.width / 2 - cy
            
            cv2.circle(cv_image, (cy, int(msg.height/2)), 7, (0,255,0), 1)
            cv2.line(cv_image, (160, 120), (cy, int(msg.height/2)), (0, 255, 0), 3)

      self.show_image(cv_image)


  def show_image(self, img, title='Camera 1'):
        cv2.imshow(title, img)
        cv2.waitKey(3)




  def enable_motors(self):
      rospy.wait_for_service("/enable_motors")
      foo2call = rospy.ServiceProxy("/enable_motors", EnableMotors)
      if foo2call(True):
         print("Motors started!")

  def takeoff_ctrl(self, z_des):

    return  kz * (ALTITUDE_DESIRED_VALUE - self.position.z) - bz * self.twist.linear.z
  
  def steering_strl(self):
      u_s =  ks * self.omega_error - bs * (self.omega_error - self.omega_error_prev) / (1.0 / 50.0)
      self.omega_error_prev = self.omega_error
      return u_s


  def y_offset_ctrl(self):
    u_y = ky * self.y_error - by * (self.y_error - self.y_error_prev) / (1.0 / 50.0)
    self.y_error_prev = self.y_error
    return u_y

  def spin(self):
     self.enable_motors()
     try:
         rate = rospy.Rate(50.0)
         while not rospy.is_shutdown():
             u_z = self.takeoff_ctrl(ALTITUDE_DESIRED_VALUE)
             u_s = self.steering_strl()
             u_y = self.y_offset_ctrl()

             cmd_msg = Twist()

             cmd_msg.linear.z = u_z
             cmd_msg.linear.x = 2.2
             cmd_msg.linear.y = u_y
             cmd_msg.angular.z = -u_s

             self.cmd_pub.publish(cmd_msg)


             rate.sleep()
     except:
         self.stop_robot()
         print("Stop the robot!")
class RingDetectionController:
    def __init__(self):
        rospy.init_node('ring_detection_controller', anonymous=True)
        self.bridge = CvBridge()
        self.z_des = DEFAULT_ALTITUDE
        self.image_1 = None
        self.image_2 = None
        self.red_ring_detected = False
        self.blue_ring_detected = False
        self.time_start_up = 0

        rospy.Subscriber('/cam_1/camera/image', Image, self.callback_camera_1)
        rospy.Subscriber('/cam_2/camera/image', Image, self.callback_camera_2)

    def callback_camera_1(self, data):
        cv_image = self.bridge.imgmsg_to_cv2(data, 'bgr8')
        self.image_1 = cv_image

    def callback_camera_2(self, data):
        cv_image = self.bridge.imgmsg_to_cv2(data, 'bgr8')
        self.image_2 = cv_image
        self.detect_rings()

    def detect_rings(self):
# Detect red ring
        lower_red = np.array([0, 100, 100])
        upper_red = np.array([10, 255, 255])
        red_mask = cv2.inRange(self.image_2, lower_red, upper_red)
        red_contours, _ = cv2.findContours(red_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for red_contour in red_contours:
            red_radius = cv2.minEnclosingCircle(red_contour)[1]
            if 50 < red_radius < 70:
                self.red_ring_detected = True
                self.blue_ring_detected = False
                self.time_start_up = rospy.get_time()

# Detect blue ring
        lower_blue = np.array([100, 100, 100])
        upper_blue = np.array([140, 255, 255])
        blue_mask = cv2.inRange(self.image_2, lower_blue, upper_blue)
        blue_contours, _ = cv2.findContours(blue_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for blue_contour in blue_contours:
            blue_radius = cv2.minEnclosingCircle(blue_contour)[1]
            if 50 < blue_radius < 80:
                self.blue_ring_detected = True
                self.red_ring_detected = False
                self.time_start_up = rospy.get_time()

    def spin(self):
        while not rospy.is_shutdown():
            if self.red_ring_detected:
                self.z_des = DEFAULT_ALTITUDE
            elif self.blue_ring_detected and rospy.get_time() - self.time_start_up < RING_AVOIDANCE_TIME:
                self.z_des = 5
            elif self.blue_ring_detected:
# Implement your logic to fly through the blue ring
                pass
            else:
                self.z_des = DEFAULT_ALTITUDE
def main():
  controller = RingDetectionController()
  controller.spin()
  rospy.spin()
  ctrl = Controller()
  ctrl.spin()
  

if __name__=="__main__":
  main()
   
     